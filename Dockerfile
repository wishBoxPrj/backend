FROM python:3.7.4

ENV PYTHONUNBUFFERED 1

WORKDIR /app

COPY poetry.lock .
COPY pyproject.toml .

RUN pip install poetry && \
    poetry config virtualenvs.create false && \
    poetry install --no-dev

ADD . .

CMD python -m manage migrate && \
    gunicorn app.config.wsgi -w 4 -b 0.0.0.0

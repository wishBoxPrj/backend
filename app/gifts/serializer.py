from rest_framework import serializers

from app.gifts.models import Gift
from app.profiles.serializer import ProfileSerializer


class GiftSerializer(serializers.ModelSerializer):
    owner = ProfileSerializer(read_only=True)
    booked_by = ProfileSerializer(read_only=True)

    class Meta:  # noqa: WPS306
        model = Gift
        fields = "__all__"


class CategorySerializer(serializers.Serializer):
    name = serializers.CharField()

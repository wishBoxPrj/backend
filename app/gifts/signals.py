from typing import Any

from app.gifts.models import CategoriesEnum, Category


def create_categories(sender: Any, **_kwargs: Any) -> None:
    for category in CategoriesEnum:
        Category.objects.get_or_create(name=category.value)

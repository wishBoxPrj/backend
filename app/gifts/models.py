from enum import Enum

from django.db import models

from app.profiles.models import Profile

NAME_MAX_LENGHT = 50
PICTURE_URL_MAX_LENGHT = 200
CATEGORY_MAX_LENGHT = 25
COMMENT_MAX_LENGHT = 500


class CategoriesEnum(Enum):
    technics = "technics"
    children_products = "children_s_products"
    auto = "auto"
    household_products = "household_products"
    sport_and_recreation = "sports_and_recreation"
    cosmetics = "cosmetics"
    food = "food"
    wardrobe = "wardrobe"
    hobby = "hobby"
    books = "books"
    accessories = "accessories"
    undefined = "undefined"


class Category(models.Model):
    name = models.CharField(
        primary_key=True,
        max_length=CATEGORY_MAX_LENGHT,
        choices=[(cat.value, cat.value) for cat in CategoriesEnum],
    )

    def __str__(self) -> str:
        return self.name


class Gift(models.Model):
    name = models.CharField(max_length=NAME_MAX_LENGHT)
    picture = models.CharField(max_length=PICTURE_URL_MAX_LENGHT, null=True)
    comment = models.CharField(max_length=COMMENT_MAX_LENGHT, null=True)
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, default=CategoriesEnum.undefined
    )
    priority = models.SmallIntegerField()
    booked_by = models.ForeignKey(
        Profile, related_name="booked_gifts", on_delete=models.SET_NULL, null=True
    )
    booking_time = models.DateTimeField(null=True)
    owner = models.ForeignKey(Profile, related_name="gifts", on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.name

    class Meta:  # noqa: WPS306
        ordering = ("-id",)

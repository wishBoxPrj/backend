from django.apps import AppConfig
from django.db.models.signals import post_migrate


class GiftsConfig(AppConfig):
    name = "app.gifts"
    label = "gifts"
    verbose_name = "Gifts"

    def ready(self) -> None:
        import app.gifts.signals  # noqa: WPS433, WPS301

        post_migrate.connect(app.gifts.signals.create_categories, sender=self)


default_app_config = "app.gifts.GiftsConfig"

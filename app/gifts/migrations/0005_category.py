# Generated by Django 2.2.7 on 2019-11-25 13:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("gifts", "0004_auto_20191125_1316")]

    operations = [
        migrations.CreateModel(
            name="Category",
            fields=[
                (
                    "name",
                    models.CharField(max_length=20, primary_key=True, serialize=False),
                )
            ],
        )
    ]

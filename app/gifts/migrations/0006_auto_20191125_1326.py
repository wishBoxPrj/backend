# Generated by Django 2.2.7 on 2019-11-25 13:26

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("gifts", "0005_category")]

    operations = [
        migrations.AlterField(
            model_name="gift",
            name="category",
            field=models.ForeignKey(
                default="Undefined",
                on_delete=django.db.models.deletion.CASCADE,
                to="gifts.Category",
            ),
        )
    ]

from django.contrib import admin

from app.gifts.models import Category, Gift

admin.site.register(Gift)
admin.site.register(Category)

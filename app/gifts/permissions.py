from django.views import View
from rest_framework.permissions import SAFE_METHODS, BasePermission
from rest_framework.request import Request


class IsOwnerOrReadOnly(BasePermission):
    def has_permission(self, request: Request, view: View) -> bool:
        if request.method in SAFE_METHODS:
            return True
        return view.kwargs.get("nickname") == str(request.user.social_profile)


class IsOwner(BasePermission):
    def has_permission(self, request: Request, view: View) -> bool:
        return view.kwargs.get("nickname") == str(request.user.social_profile)

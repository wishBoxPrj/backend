from typing import Any

from django import http, utils
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.generics import (
    ListAPIView,
    ListCreateAPIView,
    RetrieveDestroyAPIView,
    RetrieveUpdateDestroyAPIView,
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response

from app.gifts.models import Category, Gift
from app.gifts.permissions import IsOwnerOrReadOnly
from app.gifts.serializer import CategorySerializer, GiftSerializer
from app.profiles.models import Profile


class GiftsListCreate(ListCreateAPIView):
    serializer_class = GiftSerializer
    permission_classes = (IsAuthenticated, IsOwnerOrReadOnly)

    def post(self, request: Request, nickname: str) -> Response:
        profile = request.user.social_profile
        gift = self.get_serializer(data=request.data)
        gift.is_valid(raise_exception=True)
        gift.save(owner=profile)
        return Response(data=gift.data, status=status.HTTP_200_OK)

    def get(self, request: Request, nickname: str) -> Response:
        profile = get_object_or_404(Profile, nickname=nickname)
        gifts = profile.gifts
        serializer = self.get_serializer(gifts, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class GiftDetails(RetrieveUpdateDestroyAPIView):
    serializer_class = GiftSerializer
    queryset = Gift.objects.all()
    permission_classes = (IsAuthenticated,)

    def delete(self, request: Request, pk: int) -> Response:
        gift = get_object_or_404(Gift, pk=pk)
        if gift.owner == request.user.social_profile:
            gift.delete()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_403_FORBIDDEN)

    def patch(self, request: Request, pk: int) -> Response:
        gift = get_object_or_404(Gift, pk=pk)

        if gift.owner == request.user.social_profile:
            serializer = self.get_serializer(gift, data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_403_FORBIDDEN)


class BookedGifts(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = GiftSerializer

    def get_queryset(self) -> Response:
        profile = self.request.user.social_profile
        return Gift.objects.filter(booked_by=profile).order_by("-booking_time")


class BookedGiftDetails(RetrieveDestroyAPIView):
    serializer_class = GiftSerializer
    queryset = Gift.objects.all()
    permission_classes = (IsAuthenticated,)

    def delete(self, request: Request, pk: int) -> Response:
        profile = request.user.social_profile
        try:
            gift = profile.booked_gifts.get(pk=pk)
        except Gift.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        gift.booked_by = None
        gift.booking_time = None
        gift.save()
        return Response(status=status.HTTP_200_OK)

    def post(self, request: Request, pk: int) -> Response:
        gift = get_object_or_404(Gift, pk=pk)
        if gift.booked_by is None:
            gift.booked_by = request.user.social_profile
            gift.booking_time = utils.timezone.now()  # noqa: WPS601
            gift.save()
            serializer = self.get_serializer(gift)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        return Response(
            data={"error": "This gift already booked"},
            status=status.HTTP_406_NOT_ACCEPTABLE,
        )


class CategoryList(ListAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()

    def get(self, request: Request) -> Any:
        categories = self.get_queryset()
        cat_list = [cat.name for cat in categories]
        return http.JsonResponse(cat_list, safe=False)

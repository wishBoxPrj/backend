from django.urls import path

from app.gifts.views import (
    BookedGiftDetails,
    BookedGifts,
    CategoryList,
    GiftDetails,
    GiftsListCreate,
)

urlpatterns = [
    path("booked/me", BookedGifts.as_view()),
    path("booked/<int:pk>", BookedGiftDetails.as_view()),
    path("categories", CategoryList.as_view()),
    path("<int:pk>", GiftDetails.as_view()),
    path("<str:nickname>", GiftsListCreate.as_view()),
]

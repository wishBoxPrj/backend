from typing import Any

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

USER_NICKNAME_MAX_LENGTH = 15
PHOTO_URL_MAX_LENGTH = 200


class Profile(models.Model):
    owner = models.OneToOneField(
        User, primary_key=True, on_delete=models.CASCADE, related_name="social_profile"
    )
    nickname = models.CharField(
        max_length=USER_NICKNAME_MAX_LENGTH, unique=True, null=True
    )
    date_of_birth = models.DateField(null=True)
    photo = models.CharField(max_length=PHOTO_URL_MAX_LENGTH, null=True)

    def __str__(self) -> str:
        return self.nickname or "undefined"


@receiver(post_save, sender=User)
def create_user_profile(instance: User, created: bool, **_: Any) -> None:
    if created:
        Profile.objects.create(owner=instance)

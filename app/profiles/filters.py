import django_filters

from app.profiles.models import Profile


class ProfileFilter(django_filters.FilterSet):
    class Meta:  # noqa: WPS306
        model = Profile
        fields = ("nickname",)

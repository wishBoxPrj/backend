from django.urls import path

from app.profiles.views import ProfilesView, delete_user_view, profile_view

urlpatterns = [
    path("", ProfilesView.as_view()),
    path("me", profile_view),
    path("me/delete", delete_user_view),
]

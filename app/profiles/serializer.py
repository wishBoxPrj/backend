import re
from typing import Optional

from django.contrib.auth.models import User
from rest_framework import serializers

from app.profiles.models import Profile


class UserSerializer(serializers.ModelSerializer):
    class Meta:  # noqa: WPS306
        model = User
        fields = ("id", "username", "first_name", "last_name")


class ProfileSerializer(serializers.ModelSerializer):
    owner = UserSerializer(read_only=True)

    class Meta:  # noqa: WPS306
        model = Profile
        fields = ("owner_id", "owner", "nickname", "date_of_birth", "photo")

    def validate_nickname(self, nickname: str) -> Optional[str]:
        if re.match(r"^[a-z0-9_-]{4,15}$", nickname) is None:
            raise serializers.ValidationError("Must contain only latin and numbers")
        return nickname

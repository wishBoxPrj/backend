from django.contrib.auth.models import User
from rest_framework import generics, status
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response

from app.friends.models import Friend, FriendshipRequest
from app.gifts.models import Gift
from app.profiles.filters import ProfileFilter
from app.profiles.models import Profile
from app.profiles.serializer import ProfileSerializer


@api_view(["GET", "PATCH"])
def profile_view(request: Request) -> Response:
    profile = request.user.social_profile
    serializer = ProfileSerializer(profile)

    if request.method == "PATCH":
        serializer = ProfileSerializer(profile, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()

    return Response(data=serializer.data, status=status.HTTP_200_OK)


@api_view(["DELETE"])
def delete_user_view(request: Request) -> Response:
    profile = request.user.social_profile
    Friend.objects.filter(to_user=profile).delete()
    Friend.objects.filter(from_user=profile).delete()
    FriendshipRequest.objects.filter(to_user=profile).delete()
    FriendshipRequest.objects.filter(from_user=profile).delete()
    Gift.objects.filter(owner=profile).delete()
    profile.delete()
    User.objects.get(username=request.user).delete()
    return Response(status=status.HTTP_200_OK)


class ProfilesView(generics.ListAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = (IsAuthenticated,)
    filterset_class = ProfileFilter

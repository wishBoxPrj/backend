from django.contrib import admin

from app.profiles.models import Profile

admin.site.register(Profile)

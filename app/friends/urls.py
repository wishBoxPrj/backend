from django.urls import path

from app.friends.views import (
    accept_friend_view,
    add_friend_view,
    delete_friend_view,
    friends_list_view,
    reject_friend_view,
    requests_list_view,
    sent_requests_view,
)

urlpatterns = [
    path("friends", friends_list_view),
    path("requests", requests_list_view),
    path("add", add_friend_view),
    path("response/accept", accept_friend_view),
    path("response/reject", reject_friend_view),
    path("delete", delete_friend_view),
    path("sent", sent_requests_view),
]

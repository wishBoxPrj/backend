from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.request import Request
from rest_framework.response import Response

from app.friends.models import Friend, FriendshipRequest
from app.friends.serializer import ProfileInRequestSerializer
from app.profiles.models import Profile
from app.profiles.serializer import ProfileSerializer

NICKNAME = "nickname"


@api_view(["GET"])
def friends_list_view(request: Request) -> Response:
    profile = Profile.objects.get(owner=request.user)
    friends = Friend.objects.friends(profile)
    serializer = ProfileSerializer(friends, many=True)
    return Response(data=serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
def requests_list_view(request: Request) -> Response:
    profile = Profile.objects.get(owner=request.user)
    friends = Friend.objects.unread_requests(profile)
    nicknames = [person for person in friends]
    profiles = Profile.objects.filter(nickname__in=nicknames)
    serializer = ProfileSerializer(profiles, many=True)
    return Response(data=serializer.data, status=status.HTTP_200_OK)


@api_view(["POST"])
def add_friend_view(request: Request) -> Response:
    from_user = Profile.objects.get(owner=request.user)
    serializer = ProfileInRequestSerializer(request.data)
    to_user = Profile.objects.get(nickname=serializer.data[NICKNAME])
    Friend.objects.add_friend(from_user, to_user)
    profile_serializer = ProfileSerializer(to_user)
    return Response(data=profile_serializer.data, status=status.HTTP_201_CREATED)


@api_view(["POST"])
def accept_friend_view(request: Request) -> Response:
    from_user = Profile.objects.get(owner=request.user)
    serializer = ProfileInRequestSerializer(request.data)
    profile_to_accept = Profile.objects.get(nickname=serializer.data[NICKNAME])
    friend_request = FriendshipRequest.objects.get(
        to_user=from_user, from_user=profile_to_accept
    )
    friend_request.accept()
    profile_serializer = ProfileSerializer(profile_to_accept)
    return Response(data=profile_serializer.data, status=status.HTTP_202_ACCEPTED)


@api_view(["POST"])
def reject_friend_view(request: Request) -> Response:
    from_user = Profile.objects.get(owner=request.user)
    serializer = ProfileInRequestSerializer(request.data)
    profile_to_accept = Profile.objects.get(nickname=serializer.data[NICKNAME])
    friend_request = FriendshipRequest.objects.get(
        to_user=from_user, from_user=profile_to_accept
    )
    friend_request.reject()
    profile_serializer = ProfileSerializer(profile_to_accept)
    return Response(data=profile_serializer.data, status=status.HTTP_202_ACCEPTED)


@api_view(["DELETE"])
def delete_friend_view(request: Request) -> Response:
    from_user = Profile.objects.get(owner=request.user)
    serializer = ProfileInRequestSerializer(request.data)
    to_user = Profile.objects.get(nickname=serializer.data[NICKNAME])
    Friend.objects.remove_friend(from_user, to_user)
    profile_serializer = ProfileSerializer(to_user)
    return Response(data=profile_serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
def sent_requests_view(request: Request) -> Response:
    profile = request.user.social_profile
    sent = Friend.objects.sent_requests(profile=profile)
    nicknames = [person for person in sent]
    profiles = Profile.objects.filter(nickname__in=nicknames)
    serializer = ProfileSerializer(profiles, many=True)
    return Response(data=serializer.data)

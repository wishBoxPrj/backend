from django.contrib import admin

from app.friends.models import Friend, FriendshipRequest

admin.site.register(Friend)
admin.site.register(FriendshipRequest)

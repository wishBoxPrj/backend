from rest_framework import serializers


class ProfileInRequestSerializer(serializers.Serializer):
    nickname = serializers.CharField()

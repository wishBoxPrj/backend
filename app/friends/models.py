from typing import Any, List

from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from django.db.models.query import QuerySet
from django.utils import timezone

from app.friends.exceptions import AlreadyExistsError, AlreadyFriendsError
from app.friends.signals import (
    friendship_removed,
    friendship_request_accepted,
    friendship_request_canceled,
    friendship_request_created,
    friendship_request_rejected,
    friendship_request_viewed,
)
from app.profiles.models import Profile

FROM_USER = "from_user"
TO_USER = "to_user"


class FriendshipRequest(models.Model):
    """ Model to represent friendship requests """

    from_user = models.ForeignKey(
        Profile, on_delete=models.CASCADE, related_name="friendship_requests_sent"
    )
    to_user = models.ForeignKey(
        Profile, on_delete=models.CASCADE, related_name="friendship_requests_received"
    )

    created = models.DateTimeField(auto_now_add=True)
    rejected = models.DateTimeField(null=True)
    viewed = models.DateTimeField(null=True)

    def __str__(self) -> str:
        return str(self.from_user)

    def accept(self) -> None:
        """ Accept this friendship request """
        Friend.objects.create(from_user=self.from_user, to_user=self.to_user)
        Friend.objects.create(from_user=self.to_user, to_user=self.from_user)

        friendship_request_accepted.send(
            sender=self, from_user=self.from_user, to_user=self.to_user
        )

        self.delete()
        FriendshipRequest.objects.filter(
            from_user=self.to_user, to_user=self.from_user
        ).delete()

    def reject(self) -> None:
        """ reject this friendship request """
        self.rejected = timezone.now()  # noqa: WPS601
        self.save()
        friendship_request_rejected.send(sender=self)
        self.delete()

        FriendshipRequest.objects.filter(
            from_user=self.to_user, to_user=self.from_user
        ).delete()

    def cancel(self) -> None:
        """ cancel this friendship request """
        self.delete()
        friendship_request_canceled.send(sender=self)

    def mark_viewed(self) -> None:
        self.viewed = timezone.now()  # noqa: WPS601
        friendship_request_viewed.send(sender=self)
        self.save()


class FriendshipManager(models.Manager):  # noqa: WPS214
    """ Friendship manager """

    def friends(self, profile: Profile) -> List[Profile]:
        """ Return a list of all friends """
        qs = (
            Friend.objects.select_related(FROM_USER, TO_USER)
            .filter(to_user=profile)
            .all()
        )
        return [profile.from_user for profile in qs]

    def requests(self, profile: Profile) -> QuerySet:
        """ Return a list of friendship requests """

        return (
            FriendshipRequest.objects.select_related(FROM_USER, TO_USER)
            .filter(to_user=profile)
            .all()
        )

    def sent_requests(self, profile: Profile) -> List[Profile]:
        """ Return a list of friendship requests from user """
        qs = (
            FriendshipRequest.objects.select_related(FROM_USER, TO_USER)
            .filter(from_user=profile)
            .all()
        )
        return [request.to_user for request in qs]

    def unread_requests(self, profile: Profile) -> List[FriendshipRequest]:
        """ Return a list of unread friendship requests """

        qs = (
            FriendshipRequest.objects.select_related(FROM_USER, TO_USER)
            .filter(to_user=profile, viewed__isnull=True)
            .all()
        )
        return list(qs)

    def unrejected_requests(self, profile: Profile) -> QuerySet:
        """ All requests that haven't been rejected """

        return (
            FriendshipRequest.objects.select_related(FROM_USER, TO_USER)
            .filter(to_user=profile, rejected__isnull=True)
            .all()
        )

    def add_friend(self, from_user: Profile, to_user: Profile) -> FriendshipRequest:
        """ Create a friendship request """
        if from_user == to_user:
            raise ValidationError("Users cannot be friends with themselves")

        if self.are_friends(from_user, to_user):
            raise AlreadyFriendsError("Users are already friends")

        if self.can_request_send(from_user, to_user):
            raise AlreadyExistsError("Friendship is already requested")

        request = FriendshipRequest.objects.create(from_user=from_user, to_user=to_user)
        friendship_request_created.send(sender=request)
        return request

    def can_request_send(self, from_user: Profile, to_user: Profile) -> bool:
        """ Checks if a request was sent """
        if from_user == to_user:
            return False
        return FriendshipRequest.objects.filter(
            from_user=from_user, to_user=to_user
        ).exists()

    def remove_friend(self, from_user: Profile, to_user: Profile) -> bool:
        """ Destroy a friendship relationship """
        try:
            qs = (
                Friend.objects.filter(
                    Q(to_user=to_user, from_user=from_user)
                    | Q(to_user=from_user, from_user=to_user)  # noqa: W503
                )
                .distinct()
                .all()
            )
        except Friend.DoesNotExist:
            return False
        if qs:
            friendship_removed.send(sender=qs[0], from_user=from_user, to_user=to_user)
            qs.delete()
            return True
        return False

    def are_friends(self, user1: Profile, user2: Profile) -> bool:
        """ Are these two users friends? """
        friend1 = Friend.objects.filter(to_user=user1, from_user=user2).exists()
        friend2 = Friend.objects.filter(to_user=user2, from_user=user1).exists()
        return friend1 and friend2


class Friend(models.Model):
    """ Model to represent Friendships """

    to_user = models.ForeignKey(Profile, models.CASCADE, related_name="friends")
    from_user = models.ForeignKey(
        Profile, models.CASCADE, related_name="_unused_friend_relation"
    )
    created = models.DateTimeField(default=timezone.now)

    objects = FriendshipManager()  # noqa: WPS110

    def __str__(self) -> str:
        return "User {0} is friend for {1}".format(self.to_user_id, self.from_user_id)

    def save(self, *args: Any, **kwargs: Any) -> None:
        # Ensure users can't be friends with themselves
        if self.to_user == self.from_user:
            raise ValidationError("Users cannot be friends with themselves.")
        super(Friend, self).save(*args, **kwargs)  # noqa: WPS608

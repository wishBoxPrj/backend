from django.db import IntegrityError


class AlreadyExistsError(IntegrityError):
    pass  # noqa: WPS420, WPS604


class AlreadyFriendsError(IntegrityError):
    pass  # noqa: WPS420, WPS604

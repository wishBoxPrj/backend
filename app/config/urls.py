from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("admin/", admin.site.urls),
    path("login/", include("rest_social_auth.urls_token")),
    path("profiles/", include("app.profiles.urls")),
    path("friendship/", include("app.friends.urls")),
    path("gifts/", include("app.gifts.urls")),
]
